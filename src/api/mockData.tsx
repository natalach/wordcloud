import { getRandomNumber } from "../components/util/services/get-random-number";
import { IQuestionData } from "../models/question-data";
import { questions } from "./getQuestions";

export const getRandomQuestion = () =>
  new Promise<IQuestionData>((resolve, reject) => {
    const id = getRandomNumber(0,4)
    const question = questions[id];
    if (!question) {
      return setTimeout(
        () => reject(new Error('Question not found')),
        1000
      );
    }
    setTimeout(() => resolve(questions[id]), 1000);
  });