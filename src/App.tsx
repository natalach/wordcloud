import React, { useContext } from 'react';

import classes from './App.module.css';
import Login from './components/Login/Login'
import Question from './components/Question/Question'
import Result from './components/Result/Result'
import { AppContext } from './context/app-context';


function App() {
  const ctx = useContext(AppContext)
  return (
    <div className={classes.app}>
      {ctx.mode === 0 && <Login />}
      {ctx.mode === 1 && <Question/>}
      {ctx.mode === 2 && <Result />}
    </div>
  )
};

export default App;
