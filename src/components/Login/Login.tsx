
import { useContext, useRef, useState } from 'react';

import { AppContext } from '../../context/app-context';
import useInput from '../../hooks/use-input';
import { IError } from '../../models/error';
import Button from '../util/components/Button/Button';
import Modal from '../util/components/Modal/Modal';
import classes from './Login.module.css';


const Login = () => {
  const [error, setError] = useState<IError | null>();

  const ctx = useContext(AppContext);

  const inputRef = useRef<HTMLInputElement>(null);

  const {
    value: nameValue,
    isValid: nameisValid,
    hasError: nameHasError,
    inputHandler: nameInputHandler,
    blurHandler: nameBlurHandler
  } = useInput((val: string) => val.trim() !== '');

  const submitHandler = (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    if (!nameisValid) {
      setError({
        title: 'Invalid input',
        message: 'The name must not be empty or consist of spaces only!'
      })
      return;
    }
    ctx.changeName(nameValue);
    ctx.changeMode();
  }
  const errorHandler = () => {
    inputRef.current?.focus();
    setError(null);
  }
  return (
    <>
      {error && <Modal title={error.title} message={error.message} onConfirm={errorHandler} />}
      <form className={classes.form} onSubmit={submitHandler}>
        <div className={classes.title}>Wordcloud game</div>
        <input ref={inputRef} className={classes.input} placeholder='Enter your name here...' value={nameValue} onChange={nameInputHandler} onBlur={nameBlurHandler} type='text' id='name' />
        {nameHasError && <p className={classes.info}>Please enter a valid name</p>}
        <Button content='play' />
      </form>
    </>
  )
}

export default Login;