import React from 'react';

import { render, screen } from '@testing-library/react';

import Login from './Login';
import { AppContext } from '../../context/app-context';
import { defaultCtx } from '../../context/defaultCtx';



describe('Login component', () => {
  test('renders title', () => {
    render(<AppContext.Provider value={defaultCtx}><Login /></AppContext.Provider>);
    const linkElement = screen.getByText(/Wordcloud game/i);
    expect(linkElement).toBeInTheDocument();
  });
  
  
  test('renders button', () => {
    render(<AppContext.Provider value={defaultCtx}><Login /></AppContext.Provider>);
    const button = screen.getByRole('button');
    expect(button).toBeInTheDocument();
  });
  
})

