export const getRandomNumber = (minNum: number, maxNum: number) => {
  return Math.floor(Math.random() * (maxNum + 1 - minNum)) + minNum;
}
