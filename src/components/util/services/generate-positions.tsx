import { getRandomNumber } from './get-random-number';

const generatePositions = () => {
  let positionList: { top: string; left: string }[] = [];

  const { innerWidth: width } = window;
  const positions = new Array(24);
  for (let i = 0; i < 24; ++i) positions[i] = 0;

  const getPositionsForGrid = (rows: number, cols: number) => {
    let top: number;
    let left: number;
    while (true) {
      top = getRandomNumber(0, rows - 1);
      left = getRandomNumber(0, cols - 1);
      if (positions[cols * top + left] === 0) {
        positions[cols * top + left] = 1;
        break;
      }
    }
    const moveFromTop = getRandomNumber(0, 50);
    const moveFromLeft = rows < cols ? getRandomNumber(0, 50) : getRandomNumber(0, 20);
    const topPercentage = rows < cols ? 25 : 15;
    const leftPercentage = rows < cols ? 15 : 23;
    return { top: `calc(${(top) * topPercentage}% + ${moveFromTop}px)`, left: `calc(${(left) * leftPercentage}% + ${moveFromLeft}px)` }
  };

  const getRandPositions = () => {
    if (width > 750) {
      return getPositionsForGrid(4, 6);
    }
    return getPositionsForGrid(6, 4);
  };

  for (let i = 0; i < 24; i++) {
    const obtainedStyle = getRandPositions();
    positionList = [...positionList, obtainedStyle];
  }
  return positionList;
}

export default generatePositions
