import ReactDOM from 'react-dom';
import React from 'react';

import Button from '../Button/Button';
import styles from './Modal.module.css'


const Backdrop: React.FC<{ onConfirm: () => void }> = (props) => {
  return <div className={styles.backdrop} onClick={props.onConfirm} ></div>
};

const ModalOverlay: React.FC<{ title: string, message: string, onConfirm: any }> = (props) => {
  return (
    <div className={styles.modal}>
      <header className={styles.header}>
        <h2>{props.title}</h2>
      </header>
      <div className={styles.content}>
        <p>{props.message}</p>
      </div>
      <footer className={styles.actions}>
        <Button onClick={props.onConfirm} content='OK' />
      </footer>
    </div>
  )
};

const backdrop: HTMLElement | null = document.getElementById('backdrop-root');
const overlay: HTMLElement | null = document.getElementById('overlay-root');

const Modal: React.FC<{ title: string, message: string, onConfirm: () => void }> = (props) => {
  return (
    <React.Fragment>
      {backdrop && ReactDOM.createPortal(<Backdrop onConfirm={props.onConfirm}/>, backdrop)}
      {overlay && ReactDOM.createPortal(<ModalOverlay onConfirm={props.onConfirm} title={props.title} message={props.message}/>, overlay)}
    </React.Fragment>
  )
};

export default Modal;
