import React from 'react';

import classes from './Button.module.css';

const Button: React.FC<{ content: string, onClick?: () => void }> = (props) => {
  return (
    <button className={classes.button} onClick={props.onClick}>{props.content}</button>
  )
};

export default Button;