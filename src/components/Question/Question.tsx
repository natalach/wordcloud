import React, { useContext, useEffect, useState } from 'react';

import { getRandomQuestion } from '../../api/mockData';
import { AppContext } from '../../context/app-context';
import { IError } from '../../models/error';
import { IWord } from '../../models/question-data';
import Button from '../util/components/Button/Button';
import generatePositions from '../util/services/generate-positions';
import classes from './Question.module.css';
import icon from '../../assets/icons/shuffle.png';
import Word from './Word/Word';
import Loader from '../util/components/Loader/Loader';
import Modal from '../util/components/Modal/Modal';

const Question = () => {
  const ctx = useContext(AppContext);

  const [question, setQuestion] = useState<string>('');
  const [shouldShuffle, setShouldShuffle] = useState<boolean>(false);
  const [isLoaded, setIsLoaded] = useState<boolean>(false);

  const [words, setWords] = useState<IWord[]>([]);
  const [error, setError] = useState<IError | null>();

  const [inlines, setInlines] = useState<{ top: string; left: string }[]>([]);

  useEffect(() => {
    const positions = generatePositions();
    setInlines(positions);
    setShouldShuffle(false);
  }, [shouldShuffle]);

  useEffect(() => {
    const loadQuestions = async () => {
      try {
        const result = await getRandomQuestion();
        setQuestion(result.question);
        setWords(result.all_words.map(w => ({ word: w, isCorrect: result.good_words.includes(w), selected: false })));
        setIsLoaded(true);
      } catch (error) {
        setError({
          title: 'Error',
          message: 'Something went wrong. Try again later!'
        })
        setIsLoaded(true)
      }
    };
    loadQuestions()
  }, []);


  const handleFinishGame = () => {
    ctx.changeMode();
    ctx.setShowing();
  };

  const handleShowAnswers = () => {
    ctx.setRes(words);
    ctx.setShowing();
  };

  const handleToggle = (id: number) => {
    const newWords = [...words];
    newWords[id].selected = !newWords[id].selected;
    setWords(newWords);
  };

  const errorHandler = () => {
    ctx.changeMode(0);
    setError(null);
  };

  const shuffleHandler = () => {
    setShouldShuffle(true);
  };

  return (
    <div className={classes['question-box']}>
      {error && <Modal title={error.title} message={error.message} onConfirm={errorHandler} />}
      {!isLoaded && <Loader />}
      {isLoaded &&
        <>
          <div className={classes.question}>
            <div className={classes.question__content}>
              {question}
            </div>
            <button onClick={shuffleHandler} title='shuffle' className={classes.question__icon}>
              <img src={icon} alt='shuffle' />
            </button>
          </div>
          <div className={classes['word-box']}>
            {words.map((w, id) => <div className={classes.word} key={Math.random()} style={inlines[id]}><Word word={w} id={id} onToggle={handleToggle} /></div>)}
          </div>
          {!ctx.show && <Button content='check answers' onClick={handleShowAnswers} />}
          {ctx.show && <Button content='finish game' onClick={handleFinishGame} />}
        </>}
    </div>
  )
};

export default Question;