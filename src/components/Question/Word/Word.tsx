import React, { useContext, useEffect, useState } from 'react';

import { AppContext } from '../../../context/app-context';
import { IWord } from '../../../models/question-data';
import classes from './Word.module.css';

const Word: React.FC<{ word: IWord, id: number, onToggle: (id: number) => void }> = (props) => {
  const [ansCorrect, setAnsCorrect] = useState<number>(0);
  const ctx = useContext(AppContext);
  const handleClick = () => {
    !ctx.show && props.onToggle(props.id);
  }


  useEffect(() => {
    if (ctx.show) {
      if (props.word.selected && props.word.isCorrect) {
        setAnsCorrect(1);
      }
      if (props.word.selected ? !props.word.isCorrect : props.word.isCorrect) {
        setAnsCorrect(2);
      }
    }
  }, [ctx.show, props.word.selected, props.word.isCorrect]);

  const answerClass = `${ansCorrect === 1 ? classes.correct : ansCorrect === 2 ? classes.wrong : ''}`;

  const answerRevealedClasses = `${classes.word}`;
  const answerHiddenClasses = `${classes.word} ${classes.wordToSelect} ${props.word.selected ? classes.clicked : ''}`;
  const mainClasses = ctx.show ? answerRevealedClasses : answerHiddenClasses;
  return (
    <div className={mainClasses} onClick={handleClick}>
      {ansCorrect === 1 && <div className={answerClass}>Good</div>}
      <div>{props.word.word}</div>
      {ansCorrect === 2 && <div className={answerClass}>Bad</div>}
    </div>
  )
}

export default Word;