import React, { useContext } from 'react';
import { AppContext } from '../../context/app-context';
import Button from '../util/components/Button/Button';
import classes from './Result.module.css'

const Result = () => {
  const ctx = useContext(AppContext);
  const handleClick = () => {
    ctx.changeMode();
  };
  return (
    <div className={classes.result}>
      <div>
        Congratulations, {ctx.name}!
      </div>
      <div>Your score:</div>
      <div className={classes.score}>{ctx.result} points</div>
      <Button content='new game' onClick={handleClick} />
    </div>
  )
};

export default Result;