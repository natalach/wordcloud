import React from 'react';
import { render, screen } from '@testing-library/react';
import Result from './Result';


describe('Result component', () => {
  test('renders text', () => {
    render(<Result />);
    const text = screen.getByText(/Your score:/i);
    expect(text).toBeInTheDocument();
  });

  test('renders result', () => {
    render(<Result />);
    const text = screen.getByText(/0 points/i);
    expect(text).toBeInTheDocument();
  });


  test('renders button', () => {
    render(<Result />);
    const button = screen.getByRole('button');
    expect(button).toBeInTheDocument();
  });
  
})

