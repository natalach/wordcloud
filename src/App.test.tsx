import React from 'react';

import { render, screen } from '@testing-library/react';
import App from './App';
import { AppContext } from './context/app-context';
import { defaultCtx } from './context/defaultCtx';


jest.mock("./components/Login/Login", () => {
  return () => <div>Login</div>
});

jest.mock("./components/Question/Question", () => {
  return () => <div>Question</div>
});

jest.mock("./components/Result/Result", () => {
  return () => <div>Result</div>
});

describe('App component', () => {
  test('renders learn react link', () => {
    const specificMockCtx = { ...defaultCtx, mode: 0 }

    render(<AppContext.Provider value={specificMockCtx}><App /></AppContext.Provider>);
    const linkElement = screen.getByText(/Login/i);
    expect(linkElement).toBeInTheDocument();
  });


  test('renders learn react link', () => {
    const specificMockCtx = { ...defaultCtx, mode: 1 }

    render(<AppContext.Provider value={specificMockCtx}><App /></AppContext.Provider>);
    const linkElement = screen.getByText(/Question/i);
    expect(linkElement).toBeInTheDocument();
  });


  test('renders learn react link', () => {
    const specificMockCtx = { ...defaultCtx, mode: 2 }
    render(<AppContext.Provider value={specificMockCtx}><App /></AppContext.Provider>);
    const linkElement = screen.getByText(/Result/i);
    expect(linkElement).toBeInTheDocument();
  });
})
