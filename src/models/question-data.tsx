export interface IQuestionData {
  id: number;
  question: string;
  'all_words': string[];
  'good_words': string[]
}

export interface IWord {
  word: string;
  isCorrect: boolean;
  selected: boolean;
}