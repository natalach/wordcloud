import { IWord } from "../models/question-data";

export const defaultCtx = {
  name: '',
  mode: 0,
  show: false,
  result: 0,
  changeName: (name: string) => { },
  changeMode: (mode?: number) => { },
  setShowing: () => { },
  setRes: (words: IWord[]) => { },
}