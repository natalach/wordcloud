import React, { useState } from "react";
import { IWord } from "../models/question-data";
import { defaultCtx } from "./defaultCtx";

interface IContext {
  name: string,
  mode: number,
  show: boolean,
  result: number,
  changeName: (name: string) => void,
  changeMode: (mode?: number) => void,
  setShowing: () => void,
  setRes: (words: IWord[]) => void
}

export const AppContext = React.createContext<IContext>(defaultCtx);

const Provider: React.FC<{}> = (props) => {

  const [name, setName] = useState<string>('')
  const [mode, setMode] = useState<number>(0)
  const [result, setResult] = useState<number>(0)
  const [show, setShow] = useState<boolean>(false)

  const changeName = (name: string) => {
    setName(name);
  };

  const changeMode = (mode = -1) => {
    if (mode !== -1) {
      setMode(mode);
      return;
    }
    setMode(currentMode => {
      const nextMode = currentMode < 2 ? currentMode + 1 : 1;
      return nextMode;
    })
  };

  const setShowing = () => {
    setShow(prev => !prev);
  };

  const setRes = (words: IWord[]) => {
    const res = words.reduce((res, item) => {
      let toAdd = 0;
      if (item.selected && item.isCorrect) {
        toAdd = 2;
      }
      if (item.selected ? !item.isCorrect : item.isCorrect) {
        toAdd = -1
      }
      return res + toAdd
    }, 0);
    setResult(res);
  };

  return (
    <AppContext.Provider value={{ name, mode, show, result, changeName, changeMode, setShowing, setRes }} >
      {props.children}
    </AppContext.Provider>
  );
};

export default Provider;