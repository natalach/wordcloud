import { useState } from 'react';


const useInput = (validateValue: (value: string) => boolean) => {

  const [value, setValue] = useState('')
  const [touched, setTouched] = useState(false)

  const inputHandler = (e: any) => {
    setValue(e.target.value)
  }

  const blurHandler = (e: any) => {
    setTouched(true)
  }

  const reset = () => {
    setValue('')
    setTouched(false)
  }

  const isValid = validateValue(value)

  const hasError = !isValid && touched

  return {
    value, isValid, hasError, inputHandler, blurHandler, reset
  }

}

export default useInput
