# Wordcloud game

Welcome to [Wordcloud](https://wordcloud-three.vercel.app/) game!

Enter your name and try yourself in our game in which your task is to choose words from a word cloud. Given a question and a set of words, chooseyour answers and check the result. 

As for the implementation...

I implemented a small mock api to get the questions. It has 75% chance to fetch a question, otherwise throws an error.

I used context, as the app is small and the changes are not that frequent, but I also considered redux. I left fetching data in the Question component, otherwise I could move it somewhere (redux thunk). 

There is one custom hook - when I started implementing the app I made one for a form input logic, forgetting that the whole app has just one input... But I didn't get rid of it, it coult be used in the 'future'.

My way of distributing words in the cloud just pretends to make it randomly - there is a grid 4x6 (or 6x4 for mobile devices) of words, and each gets one cell for itself, being also moved randomly up to 50px to the bottom/right. It may happen that the words occur on top of each other, so I added a button which allows to shuffle the words.

Also, after seeing the result one may click a button to play again - this time they don;t need to provide their name.

The whole app is not optimal. I thought about using useContext, useMemo, but.. it's a small app and those hooks also take time and memory to work. So I didn't use them, having components which don't have many children.

I would use path aliases, but.. run out of time to configure that :) 

Regarding unit tests, I have written a few basix ones, as I'm new to Jest - on the everyday basis I use Jasmine.



# Getting Started with Create React App

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `yarn start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.

### `yarn test`

Launches the test runner in the interactive watch mode.\
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `yarn build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `yarn eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.

## Learn More

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).
